#  README #

**Nigtht's Watch** is a Telegram chat bot that reports Heath Statistics of the server.  It reports CPU, RAM usage, reports the process ids and the memory they consume.  It provides simple commands to configure the interval for reporting of the statistics.

### What is this repository for? ###

Design,   develop,   test   and   deploy   a   program   with   following   functional   requirements:
-1. Notify   users   with   health   stats(CPU,   RAM)   of   a   server   every   6   hours,   also   there   should   be a   command   for   serving   stats   on   demand.
-2. Notify   multiple   users   in   case   a   VM   is   not   responding   for   more   than   5s.
-3. Get   running   processes   list   on   request.
-4. APIs   must   be   authenticated   using   a   secure   stateless   token
-5. Write   unit   tests   for   all   functions
-6. Bonus:
	-1. Run   commands   on   server   from   API.
	-2. Get   notifications   via   call   or   sms

### How do I get set up? ###

######  Setting up the repo 
- Clone or download this repo, and get inside the cloned directory and  execute `npm install` to set up the required modules.
- execute `npm start` command to start the server
- Th package aleardy contains **ngrock**, it creates https url and setups the telegram web hook by itself.


###### Adding Bot to Telegram 
Got to [Night's Watch Telegram Bot](https://t.me/taketheblackbot "Night's Watch Telegram Bot") or search in Telegram @taketheblackbot

###### Testing
Get inside the folder and  execute `npm test`, it automatically tests all the functions of all modules.


