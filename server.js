const express = require('express');
const bodyParser = require('body-parser');
const fetch = require("node-fetch");
const status = require("http-status");
const ngrok = require('ngrok');
const app = express();


const Config = require("./Config/config.json");
const bot = require("./bot");

const modules = require("./Modules/main");
const Cron = modules.Cron;
const Auth = modules.Auth;


const setWebhookUrl = `${Config.TELEGRAM_API}${Config.TELEGRAM_BOT_TOKEN}/setWebhook?url=`;
const delWebhookUrl = `${Config.TELEGRAM_API}${Config.TELEGRAM_BOT_TOKEN}/deleteWebhook?url=`;

//Set the Webhook URL
ngrok.connect(Config.port,function (err, url) {
 
  //generate jsonweb token and set it to url
  let secret_pattern = "_-";
  token= Auth.genToken(secret_pattern);
  url = `${url}/webhook?token=${token}`;

  fetch(setWebhookUrl+url)
  .then(response => {return response.json();})
  .then(json => { console.log(`${json.description} on ${url}`)});

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  
  app.post('/webhook', function (req, res) {
    let token = req.query.token;
    
    let verified = Auth.verifyToken(secret_pattern,token);

    if(verified){
      let json = req.body.message || req.body.edited_message;
      bot(json);
      res.status(status.OK).send("");
    }else{
      res.status(status.BAD_REQUEST).send("");
    }
  
  
  });
  
  console.log(`Runnig at ${Config.port}`);
  app.listen(Config.port);
  
  
  //delete the webhook url on ctrl +c
  process.on('SIGINT',_ =>{
    fetch(delWebhookUrl+url)
    .then(response => {return response.json();})
    .then(json => { console.log(json.description);
      process.exit();
    });
    ngrok.disconnect(); 
  });


});
