var shell = require('shelljs');
var ps = require('current-processes');

// var json = topparser.parse(data,5);

function top(num=5){
    num = parseInt(num);

    return new Promise(function(resolve, reject){
        ps.get(function(err, processes) {
            if(err) reject(err);
            let plist = processes.slice(0,num);
            plist.map(p =>{
                p.mem = p.mem.usage;
            });
            resolve(plist);
        });
    })
}

module.exports = top;

// (function(){
//     console.log("TESTING TOP");
//     top().then(function(out){
//         let op = ``;
//         for(p of out){
//         op = op + `pid:${p.pid}, name:${p.name}, mem:${p.mem} \n`;
//         }
//         console.log(op)
//     }).catch(function(err){
//         console.log(err)
//     });
// })();

