var shell = require('shelljs');
var Config = require("../../Config/config.json");

function execute_commands(command){
    let main_command =  command.split(" ")[0];
    //if command is not allowed
    if(main_command == ""||/\s+/.test(main_command))
        return Promise.reject(`Please enter a command`)
        
    else if(!Config.allowed_commands.includes(main_command))
        return Promise.reject(`Command ${main_command} not allowed`)

    return new Promise(function(resolve, reject){
        shell.exec(command, function(code, stdout, stderr) {
            // console.log(code, stdout, stderr);
            if(stderr){
                reject(stderr)
            }
            resolve(stdout)
          });
    })
}

module.exports = execute_commands;

// (function(){
//     console.log("TESTING COMMAND EXECUTION");
//     execute_commands("echo hi").then(function(out){
//         console.log(out);
//     }).catch(function(err){
//         console.log(err);
//     });
// })();

