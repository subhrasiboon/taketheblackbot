//Bot Message
const sendMessage = require("./BotMessage/sendMessage");

//Notification
const notification = require("./Notification/sms");

//Health Stats
const ramUsage = require("./HealthStats/ramUsage");
const cpuUsage = require("./HealthStats/cpuUsage");

//Shell
const top = require("./Shell/top");
const commands = require("./Shell/commands");

//DB
const createUser = require("./DB/createUser");
const userExsists = require("./DB/userExsists");
const getUsers = require("./DB/getUsers");
const getUser = require("./DB/getUser");
const getUserToUpdate = require("./DB/getUsersToUpdate");
const setNotificationFlag = require("./DB/setNotificationFlag");
const setPhoneNumber = require("./DB/setPhNumber");
const setUpdateFlag = require("./DB/setUpdateFlag");
const setInterval = require("./DB/setInterval");
const updateTimestamp = require("./DB/updateTimestamp");

//Cron
const cron = require("./Cron/cron");

//Auth
const genToken = require("./Auth/genToken");
const verifyToken = require("./Auth/verifyToken");

module.exports ={
    Auth:{
        genToken:genToken,
        verifyToken:verifyToken
    },
    BotMessage:{
        sendMessage:sendMessage
    },
    Cron:{
        cron:cron
    },
    DB:{
        createUser:createUser,
        userExsists:userExsists,
        getUsers:getUsers,
        getUser:getUser,
        setNotificationFlag:setNotificationFlag,
        setPhoneNumber:setPhoneNumber,
        setUpdateFlag:setUpdateFlag,
        setInterval:setInterval,
        updateTimestamp:updateTimestamp,
        getUserToUpdate:getUserToUpdate 
    },
    HealthStats:{
        cpuUsage:cpuUsage,
        ramUsage:ramUsage,
    },
    Notification:{
        notification:notification
    },
    Shell:{
        top:top,
        commands:commands
    },
}