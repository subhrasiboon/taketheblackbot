var os 	= require('os-utils');

function cpuUsage(){

    return new Promise(function(resolve,reject){
        os.cpuUsage(function(v){
            resolve(v);
        });
    });
}

module.exports = cpuUsage


