function setUpdateFlag(db,userid,flag){
    return new Promise(function(resolve,reject){
        db.update({senderId:userid},{$set:{updateFlag:flag}},function(err,docs){
            if(err) reject(err)
            resolve(docs);
        });
       })
 }
 
 module.exports = setUpdateFlag;