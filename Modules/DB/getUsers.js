function getUsers(db) {
    return new Promise(function (resolve, reject) {
        db.find({}, function (err, docs) {
            if (err) reject(err)
            resolve(docs);
        });
    })

}

module.exports = getUsers;