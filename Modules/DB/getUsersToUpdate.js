function findUsers(db) {
    let currentTime = Math.floor(Date.now() / 1000);
    return new Promise(function (resolve, reject) {
        //for testing change $lte to $gte
        db.find({ nextUpdateTimestamp: { $lte: currentTime } }, function (err, docs) {
            console.log(`time:${currentTime}, probable user to update ${docs.length}`);
            if (err) reject(err);
            resolve(docs)
        });
    });
}

module.exports = findUsers;