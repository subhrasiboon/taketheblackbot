function setInterval(db,userid,interval){
    return new Promise(function(resolve,reject){
        db.update({senderId:userid},{$set:{
            updateInterval:interval,
            updateFlag:true,
            lastUpdateTimestamp:Math.floor(Date.now() / 1000),
            nextUpdateTimestamp:Math.floor(Date.now() / 1000) + interval*3600,
        }},{upsert:true},function(err,docs){
            if(err) reject(err)
            resolve(docs);
        });
       })
 }
 
 module.exports = setInterval;