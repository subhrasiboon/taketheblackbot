function setFlag_Notif(db,userid,flag){
    return new Promise(function(resolve,reject){
        db.update({senderId:userid},{$set:{notifyFlag:flag}},function(err,docs){
            if(err) reject(err)
            resolve(docs);
        });
       })
 }
 
 module.exports = setFlag_Notif;