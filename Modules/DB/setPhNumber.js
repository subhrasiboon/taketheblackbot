function setPhNum(db,userid,ph){
    return new Promise(function(resolve,reject){
        db.update({senderId:userid},{$set:{
            phoneNum:ph,
            notifyFlag:true
        }},function(err,docs){
            if(err) reject(err)
            resolve(docs);
        });
       })
 }
 
 module.exports = setPhNum;