function getUser(db, userid) {
    return new Promise(function (resolve, reject) {
        db.findOne({ senderId: userid }, function (err, doc) {
            if (err) reject(err)
            resolve(doc);
        });
    })

}

module.exports = getUser;