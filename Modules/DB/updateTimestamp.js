function updateTimestamp(db,userid,lastTimestamp,nextTimestamp){
    return new Promise(function(resolve,reject){
        db.update({senderId:userid},{$set:{
            lastUpdateTimestamp:lastTimestamp,
            nextUpdateTimestamp:nextTimestamp,
        }},function(err,docs){
            if(err) reject(err)
            resolve(docs);
        });
       })
 }
 
 module.exports = updateTimestamp;