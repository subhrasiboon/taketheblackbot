const Config = require("../../Config/config.json");

const client = require('twilio')(Config.TWILIO_SID, Config.TWILIO_TOKEN); 

function sms(phone_num,text){
    return client.messages.create({ 
         to:phone_num, 
         from: Config.TWILIO_PHONE_FROM, 
         body:text, 
     });
 }


module.exports = sms;

  // sms("+918763175632","winter is comming").then(r => console.log(r));

