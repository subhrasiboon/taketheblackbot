
function chron(db,modules) {
    const BotMessage = modules.BotMessage;
    const HealthStats = modules.HealthStats;
    const Notification = modules.Notification;
    const Db = modules.DB;

    Db.getUserToUpdate(db).then(docs => {
     
        for (u of docs) {
            let currentTime = Math.floor(Date.now() / 1000);
            //for testing
            // let nextTime = currentTime + u.updateInterval * 3600;
            let nextTime = currentTime + u.updateInterval + 3;
           

            if (u.updateFlag) {
                let UpdatePromise = Db.updateTimestamp(db, u.sender_id, currentTime, currentTime, nextTime);
                
                Promise.all([HealthStats.cpuUsage(), HealthStats.ramUsage(), UpdatePromise]).then(function (usage) {
                    let message = `Update Health Stats: \n CPU:${usage[0]}%,\n RAM: ${usage[1]}%`;
                    BotMessage.sendMessage(u.senderId, message);

                    if (u.notifyFlag) {
                       let twilior =  Notification.notification(u.phoneNum,message);
                       BotMessage.sendMessage(u.senderId, "SMS Status"+ twilior.state);
                       console.log(twilior);
                     }

                }).catch(function(errValues){
                    console.log(errValues);
                });

            }

            

        }
    }).catch(err =>{
        console.log(err);
    })


}


module.exports = chron;