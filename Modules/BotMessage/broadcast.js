const Config = require("../../Config/config.json");
const fetch = require("node-fetch");

function broadcastMessages(db,message_text){
    db.find({},function(err,docs){
        for(u of docs){
            const replyUrl = `${Config.TELEGRAM_API}${Config.TELEGRAM_BOT_TOKEN}/sendMessage?chat_id=${u.sender_id}&text=${message_text}`;
            return fetch(replyUrl);
        }
    });
   
}

module.exports = broadcastMessages;