const Config = require("../../Config/config.json");
const fetch = require("node-fetch");

function sendMessage(sender_id,message_text){
    const replyUrl = `${Config.TELEGRAM_API}${Config.TELEGRAM_BOT_TOKEN}/sendMessage?chat_id=${sender_id}&text=${message_text}`;
    return fetch(replyUrl);
}

module.exports = sendMessage;