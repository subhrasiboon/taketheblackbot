const Config = require("../Config/config.json")
const modules = require("../Modules/main");
const BotMessage = modules.BotMessage;

describe('Bot Message', function() {
    let senderId = Config.test.senderId;
    let testmsg = Config.test.testmsg;

  it('It should send message',function(done){
   BotMessage.sendMessage(senderId,testmsg)
    .then(function(r){
        if(r.status == 200)done();
    }).catch(function(err){
      done(err);
    })

  });


});
