const modules = require("../Modules/main");
const Config = require("../Config/config.json")
const Db = modules.DB;

var Datastore = require('nedb'), testusers = new Datastore();
testusers.ensureIndex({ fieldName: 'senderId', unique: true });

describe('Db', function () {

    let senderId = Config.test.senderId;
    let interval = Config.test.updateInterval;

    it('It should create a user', function (done) {
        Db.createUser(testusers, senderId)
            .then(function (dbres) {
                if (dbres.senderId == senderId) done();
            })
            .catch(function(err){
                done(err);
            });
    });


    it('set the update interval of a user', function (done) {

        Db.setInterval(testusers, senderId, interval)
            .then(function (res) {
                return Db.getUser(testusers, senderId)
            }).then(function (doc) {
                if (doc.updateInterval == interval) done();
            })
            .catch(function(err){
                done(err);
            });

            

    });

    it('It should get  users to update health stats in given time', function (done) {
        Db.getUserToUpdate(testusers)
            .then(function (docs) {
                if (docs[0].senderId == senderId) done()
            })
            .catch(function(err){
                done(err);
            });
    });

    it('It should check if user exsists or not', function (done) {
        Db.userExsists(testusers, senderId)
            .then(function (truth) {
                if (truth) done()
            });
    });

    it('It should fetch details a user', function (done) {
        Db.getUser(testusers, senderId)
            .then(function (user) {
                if (user.senderId == senderId) done()
            })
            .catch(function(err){
                done(err);
            });
    });

    it('It should fetch all the users', function (done) {
        Db.createUser(testusers, "test1234")
            .then(function (dbres) {
                return Db.getUsers(testusers)
            })
            .then(function (users) {
                if (users.length == 2) done();
            })
            .catch(function(err){
                done(err);
            });


    });

    it('It should set the notification flag for the user', function (done) {
        let truth = true;
        Db.setNotificationFlag(testusers, senderId, truth)
            .then(function (user) {
                return Db.getUser(testusers, senderId)
            })
            .then(function (user) {
                if (user.notifyFlag == truth) done();
            })
            .catch(function(err){
                done(err);
            });
    });

    it('It should set the phone number  for the user', function (done) {
        let ph = Config.test.testph;
        Db.setPhoneNumber(testusers, senderId, ph)
            .then(function (user) {
                return Db.getUser(testusers, senderId)
            })
            .then(function (user) {
                if (user.phoneNum == ph) done();
            })
            .catch(function(err){
                done(err);
            });
    });

    it('It should set the update flag  for the user', function (done) {
        let truth = true;
        Db.setUpdateFlag(testusers, senderId, truth)
            .then(function (user) {
                return Db.getUser(testusers, senderId)
            }).then(function (user) {
                if (user.updateFlag == truth) done();
            })
            .catch(function(err){
                done(err);
            });
    });

    it('It should set the update the timestamps for health stat delivery  for the user', function (done) {
        
        Db.getUser(testusers, senderId)
            .then(function (user) {
                let interval = user.updateInterval;
                let last = user.lastUpdateTimestamp;
                let next = last + (interval * 3600)
                return Db.updateTimestamp(testusers, senderId, last, next)
            })
            .then(function (confirmation) {
                if(confirmation) done()
            })
            .catch(function(err){
                done(err);
            });

    });


});