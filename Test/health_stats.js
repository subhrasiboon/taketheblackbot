const modules = require("../Modules/main");
const HealthStats = modules.HealthStats;

function isFloat(n){
    return Number(n) === n && n % 1 !== 0;
}

describe('Health Stats', function() {

  it('It should give CPU usage',function(done){
    HealthStats.cpuUsage()
    .then(function(usage){
        if(isFloat(usage)) done();
    })
  });

  it('It should give RAM usage',function(done){
    HealthStats.ramUsage()
    .then(function(usage){
        if(isFloat(usage)) done();
    })
  });


});
