const modules = require("../Modules/main");
const Auth = modules.Auth;


describe('Auth', function() {
    let secretPattern = "/^/";
    let token;

  it('should generate token',function(done){
    token = Auth.genToken(secretPattern);
    if(token.split(secretPattern).length==2) done()
  });

  it('should decode correct token',function(done){
      let verified = Auth.verifyToken(secretPattern,token);
      if(verified) done()

  });

  it('should not decode wrong token',function(done){
        try{
            let verified = Auth.verifyToken(secretPattern,"wrong token");
        }catch(err){
            done();
        }
      });

});
