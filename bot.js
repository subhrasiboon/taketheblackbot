const modules = require("./Modules/main");
const BotMessage = modules.BotMessage;
const HealthStats = modules.HealthStats;
const Shell = modules.Shell;
const Db = modules.DB;
const Cron = modules.Cron;

var Datastore = require('nedb'), users = new Datastore();
users.ensureIndex({ fieldName: 'senderId', unique: true });
/*
input 
 { "message_id": "310",
   "from": 
    { "id": "25029618",
      "is_bot": "false",
      "first_name": "Si16",
      "language_code": "en" },
   "chat": { "id": "25029618", "first_name": "Si16", "type": "private" },
   "date": "1511977561",
   "text": "hi" 
  };

*/

function bot(doc) {

  const commands = {
    "/stats": ["cpu", "ram", "update", ""],
    "/pids": [],
    "/notify": [],
    "/command": [],
    "/test": [],
  };

  // let message = doc.message
  // let update_id = doc.update_id;
  let sender_id = doc.chat.id;

  Db.userExsists(users, sender_id).then(function (truth) {
    console.log(truth);
    if (!truth) return Db.createUser(users, sender_id)
  }).then(function (nuser) {
    console.log("new user",nuser);
  }).catch(function(err){
    console.log(err);
  });


  let text = doc.text;

  //check if its a command or not
  let isCommand = /^\/([^@\s]+)@?(?:(\S+)|)\s?([\s\S]*)$/.test(text);

  let commands_list = Object.keys(commands);

  if (isCommand) {

    /*
      /stats   update   6 
      command =  `/stats`
      subcommand = `update
      option = `6
    */

    let command_split = text.split(" ");
    let command = command_split[0];
    let subcommand = command_split[1] || "";
    let option = command_split[2] || "";


    console.log(`command:${command},subcommand:${subcommand},option:${option}`);

    switch (command) {
      /**
       * /stats
       * get the CPU and RAM stats of the system
       */

      case "/stats":

        //check for invalid subcommands
        if (!commands["/stats"].includes(subcommand)) BotMessage.sendMessage(sender_id, `available with /stats ${commands["/stats"]}`);

        //check for if update option contains a negative number
        else if (subcommand == 'update' && (option != "off" && parseInt(option) < 1)) {
          BotMessage.sendMessage(sender_id, `available with /stats update off /stats update 6`);
        }

        else if (subcommand == 'update' && parseInt(option) > 0) {

          Db.setInterval(users, sender_id, option)
            .then(function (update) {
              BotMessage.sendMessage(sender_id, `Interval of ${option} hrs set`);
            })
            .catch(function (err) {
              BotMessage.sendMessage(sender_id, `Failed to set interval please reset`);
            });

        }

        else if (subcommand == 'update' && option == 'off') {

          Db.setUpdateFlag(users, sender_id, false)
            .then(function (update) {
              BotMessage.sendMessage(sender_id, `Updates disabled`);
            })
            .catch(function (err) {
              BotMessage.sendMessage(sender_id, `Failed disable updates please reset`);
            });

        }

        else if (subcommand == "cpu") {
          HealthStats.cpuUsage().then(function (usage) {
            let message = `CPU: ${usage}%`;
            BotMessage.sendMessage(sender_id, message)
          });
        }

        else if (subcommand == 'ram') {
          HealthStats.ramUsage().then(function (usage) {
            let message = `RAM: ${usage}%`;
            BotMessage.sendMessage(sender_id, message)
          });
        }

        else if (option === "" && subcommand === "") {
          Promise.all([HealthStats.cpuUsage(), HealthStats.ramUsage()]).then(function (usage) {
            console.log(usage);
            let message = `CPU:${usage[0]}%,\n RAM: ${usage[1]}%`;
            BotMessage.sendMessage(sender_id, message)
          })
        }


        else {
          let errMessage = `
                  available commands:
                  /stats
                  /stats cpu
                  /stats ram
                  /stats update off
                  /stats update <positive integer>`;
          BotMessage.sendMessage(sender_id, errMessage);
        }

        break;
      /**
       * /pids
       * get the process of the os
       */

      case "/pids":
        // In case of pids there is no option, its directly subcommands
        if (subcommand === "") {

          Shell.top().then(function (out) {
            let op = ``;
            for (p of out) { op = op + `pid:${p.pid}, name:${p.name}, mem:${p.mem} \n`; }
            BotMessage.sendMessage(sender_id, op);
          }).catch(function (err) {
            BotMessage.sendMessage(sender_id, op);
          });

        }

        else if (parseInt(subcommand) > 0) {

          Shell.top(subcommand).then(function (out) {
            let op = ``;
            for (p of out) { op = op + `pid:${p.pid}, name:${p.name}, mem:${p.mem} \n`; }
            BotMessage.sendMessage(sender_id, op);
          }).catch(function (err) {
            BotMessage.sendMessage(sender_id, op);
          });
        }

        else BotMessage.sendMessage(sender_id, "/pid <positive number>");

        break;

      /**
       * /notify
       * record notification choises of user
       */
      case "/notify":

        //check if sub command is a phone number or not
        let isPhoneNum = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(subcommand);

        if (isPhoneNum) {
          Db.setPhoneNumber(users, sender_id, subcommand)
            .then(function (update) {
              BotMessage.sendMessage(sender_id, `Notifications enabled on ${subcommand}`);
            })
            .catch(function (err) {
              BotMessage.sendMessage(sender_id, `Failed enable notification please reset`);
            });
        }


        else if (subcommand == "off") {
          Db.setNotificationFlag(users, sender_id, false)
            .then(function (update) {
              BotMessage.sendMessage(sender_id, `Notifications disabled`);
            })
            .catch(function (err) {
              BotMessage.sendMessage(sender_id, `Failed disable notification please reset`);
            });
        }


        else {
          let errorMsg = `
                  please use:
                  /notify off or /notify <phonenumber>
    
                  supported phone number formats:
                  (123) 456 7899
                  (123).456.7899
                  (123)-456-7899
                  123-456-7899
                  123 456 7899
                  1234567899
                  `;
          BotMessage.sendMessage(sender_id, errorMsg);
        }

        break;
      /**
       * /command
       * execute a commnad
       */

      case "/command":

        let bash_command = text.replace('/command ', '');

        Shell.commands(bash_command).then(function (out) {
          BotMessage.sendMessage(sender_id, out);
        }).catch(function (err) {
          BotMessage.sendMessage(sender_id, err);
        });

        break;
      /**
       * /test
       * test the bot
       */
      case "/test":
        BotMessage.sendMessage(sender_id, "Winter is Comming!");
        break;

      case "/start":
      let startMessage = `
        stats -Get the Health Stats, 
          /stats
          /stats cpu
          /stats ram
          /stats update 1
          /stats update off
        pids - Get the list of processes in server,
          /pids
          /pids 6
        command - Execute a command in the serve
        notify - Get notified about the stats through sms
          /notify <phone number>
          /notify off
        test - Test if server is working or not`;
        BotMessage.sendMessage(sender_id, startMessage);
        break;
      default:
        BotMessage.sendMessage(sender_id, `command not available:${command}, please give one of these ${commands_list}`);
    }

  } else {
    BotMessage.sendMessage(sender_id, `${text} is not a command:`);
  }

}

module.exports = bot;
setInterval(_ => Cron.cron(users, modules), 5000);


// test();
function test() {
  let userid = "25029618";
  let cases = [
    // "/stats ram",
    // "/stats cpu",
    // "/stats update 1",
    // "/stats update off",
    // "/stats",
    // "/stats",
    // "/command ls",
    // "/command",
    // "/command sudo",
    // "/pids 20",
    // "/pids",
    // "/pids -1",
    // "/test",
    // "/notify off",
    // "/notify",
    // "/notify -1",
    // "/notify +918763175632",
    // "/notify 8763175632",
    // "/notify 87631 75632"
  ]
  let test_json = {
    "update_id": "259493938",
    "message":
      {
        "message_id": "310",
        "from":
          {
            "id": "25029618",
            "is_bot": "false",
            "first_name": "Si16",
            "language_code": "en"
          },
        "chat": { "id": "25029618", "first_name": "Si16", "type": "private" },
        "date": "1511977561",
        "text": ""
      }
  };

  test_json.message.chat.id = Config.test.sender_id;

  for (c of cases) {
    let json = test_json.message;
    json.text = c;
    bot(json);

  }



};
