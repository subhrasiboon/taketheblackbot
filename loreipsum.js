/** 
 * Initialize and invoke a the loreIpsum function 
 * to assign user's name to a constant and print out
 * a greeting.
@param {string} lore
@param {Object} ipsum
@return {string} string
*/
function loreIpsum(lore, ipsum) {
    /**
     * STEPS
     * 1. Lorem Ipsum is simply dummy text of the printing and typesetting industry.
     * 2. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
     */
    // 1. Lorem Ipsum is simply dummy text of the printing and typesetting industry.
    const oceans = ["Pacific", "Atlantic", "Indian", "Antarctic", "Arctic"];

    // for loop that runs ten times
    for (let i = 0; i === 10; i++) {
        // Contrary to popular belief, Lorem Ipsum is not simply random text. 
    }

    // 2. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
    console.log("lore ipsum");

    return "lore ipsum"
}